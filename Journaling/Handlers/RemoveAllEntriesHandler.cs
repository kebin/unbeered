﻿using System.Threading;
using System.Threading.Tasks;
using Journaling.Notifications;
using Journaling.Requests;
using MediatR;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Queries;
using Raven.Client.Documents.Session;

namespace Journaling.Handlers
{
    public class RemoveAllEntriesHandler: IRequestHandler<RemoveAllEntriesRequest>
    {
        private readonly IMediator _mediator;
        private readonly IDocumentSession _documentSession;

        public RemoveAllEntriesHandler(IMediator mediator, IDocumentSession documentSession)
        {
            _mediator = mediator;
            _documentSession = documentSession;
        }

        public async Task<Unit> Handle(RemoveAllEntriesRequest request, CancellationToken cancellationToken)
        {
            _documentSession.Advanced.DocumentStore.Operations
                .Send(new DeleteByQueryOperation(new IndexQuery
                {
                    Query = "from Entries"
                }));
            
            await _mediator.Publish(new AllEntriesRemovedNotification(), cancellationToken);

            return Unit.Value;
        }
    }
}