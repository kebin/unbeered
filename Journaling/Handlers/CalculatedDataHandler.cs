﻿using System.Threading;
using System.Threading.Tasks;
using Journaling.Data.Model;
using Journaling.Notifications;
using MediatR;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Queries;
using Raven.Client.Documents.Session;

namespace Journaling.Handlers
{
    public class CalculatedDataHandler
        : INotificationHandler<NewEntryAddedNotification>
        , INotificationHandler<AllEntriesRemovedNotification>
    {
        private readonly IDocumentSession _documentSession;

        public CalculatedDataHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        public Task Handle(NewEntryAddedNotification notification, CancellationToken cancellationToken)
        {
            var calculatedData = _documentSession.Load<CalculatedData>(CalculatedData.GetId) ?? new CalculatedData
            {
                Id = CalculatedData.GetId
            };
            calculatedData.TotalNumberOfBeers += notification.NumOfBeers;
            _documentSession.Store(calculatedData);
            _documentSession.SaveChanges();
            return Task.CompletedTask;
        }

        public Task Handle(AllEntriesRemovedNotification notification, CancellationToken cancellationToken)
        {
            _documentSession.Advanced.DocumentStore.Operations
                .Send(new DeleteByQueryOperation(new IndexQuery
                {
                    Query = "from CalculatedDatas"
                }));

            return Task.CompletedTask;
        }
    }
}