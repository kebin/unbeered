﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Journaling.Data.Model;
using Journaling.Notifications;
using MediatR;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Queries;
using Raven.Client.Documents.Session;

namespace Journaling.Handlers
{
    public class ConsecutiveBeeringHandler
        : INotificationHandler<NewEntryAddedNotification>
        , INotificationHandler<HadBeersConsecutivelyNotification>
        , INotificationHandler<AllEntriesRemovedNotification>
    {
        private readonly IMediator _mediator;
        private readonly IDocumentSession _documentSession;
        private static int _currentStreak;
        private static DateTime? _streakStartedOn;

        public ConsecutiveBeeringHandler(IMediator mediator, IDocumentSession documentSession)
        {
            _mediator = mediator;
            _documentSession = documentSession;
        }

        public Task Handle(NewEntryAddedNotification notification, CancellationToken cancellationToken)
        {
            if (notification.NumOfBeers > 0)
            {
                if (!_streakStartedOn.HasValue)
                {
                    // this is the beginning! 
                    _streakStartedOn = notification.ForDate;
                }

                _currentStreak++;
            }
            else
            {
                // reset!
                _streakStartedOn = null;
                _currentStreak = 0;
            }

            if (_currentStreak >= 3)
            {
                _mediator.Publish(new HadBeersConsecutivelyNotification
                {
                    StreakStartedOnUtc = _streakStartedOn.Value,
                    CurrentStreak = _currentStreak,
                    OnUtc = notification.ForDate
                }, cancellationToken);
            }
            return Task.CompletedTask;
        }

        public Task Handle(HadBeersConsecutivelyNotification notification, CancellationToken cancellationToken)
        {
            var id = $"beeredconsecutively/{notification.StreakStartedOnUtc:yyyyMMdd}";
            var entry = _documentSession.Load<BeeredConsecutivelyData>(id)
                        ?? new BeeredConsecutivelyData
                        {
                            Id = id
                        };
            
            entry.Streak = notification.CurrentStreak;
            entry.FromUtc = notification.StreakStartedOnUtc;
            entry.ToUtc = notification.OnUtc;
            
            _documentSession.Store(entry);
            _documentSession.SaveChanges();
            
            return Task.CompletedTask;
        }

        public Task Handle(AllEntriesRemovedNotification notification, CancellationToken cancellationToken)
        {
            _documentSession.Advanced.DocumentStore.Operations
                .Send(new DeleteByQueryOperation(new IndexQuery
                {
                    Query = "from BeeredConsecutivelyDatas"
                }));
            
            return Task.CompletedTask;
        }
    }
}