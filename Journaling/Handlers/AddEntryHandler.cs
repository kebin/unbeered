﻿using System.Threading;
using System.Threading.Tasks;
using Journaling.Data.Model;
using Journaling.Notifications;
using Journaling.Requests;
using MediatR;
using Raven.Client.Documents.Session;

namespace Journaling.Handlers
{
    public class AddEntryHandler: IRequestHandler<AddEntryRequest>
    {
        private readonly IMediator _mediator;
        private readonly IDocumentSession _documentSession;

        public AddEntryHandler(IMediator mediator, IDocumentSession documentSession)
        {
            _mediator = mediator;
            _documentSession = documentSession;
        }

        public async Task<Unit> Handle(AddEntryRequest request, CancellationToken cancellationToken)
        {
            var entry = Entry.Create(request.ForDate, request.NumOfBeers);
            _documentSession.Store(entry);
            _documentSession.SaveChanges();

            await _mediator.Publish(new NewEntryAddedNotification
            {
                ForDate = request.ForDate,
                NumOfBeers = request.NumOfBeers
            }, cancellationToken);

            return Unit.Value;
        }
    }
}