﻿using System.Threading;
using System.Threading.Tasks;
using Journaling.Data.Model;
using Journaling.Notifications;
using MediatR;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Queries;
using Raven.Client.Documents.Session;

namespace Journaling.Handlers
{
    public class TooManyBeersHandler
        : INotificationHandler<NewEntryAddedNotification>
        , INotificationHandler<HadTooManyBeersNotification>
        , INotificationHandler<AllEntriesRemovedNotification>
    {
        private readonly IMediator _mediator;
        private readonly IDocumentSession _documentSession;

        public TooManyBeersHandler(IMediator mediator, IDocumentSession documentSession)
        {
            _mediator = mediator;
            _documentSession = documentSession;
        }

        public Task Handle(NewEntryAddedNotification notification, CancellationToken cancellationToken)
        {
            if (notification.NumOfBeers > 4)
            {
                _mediator.Publish(new HadTooManyBeersNotification
                {
                    OnUtc = notification.ForDate,
                    NumOfBeers = notification.NumOfBeers
                }, cancellationToken);

            }

            return Task.CompletedTask;
        }

        public Task Handle(HadTooManyBeersNotification notification, CancellationToken cancellationToken)
        {
            var entry = TooManyBeersData.Create(notification.NumOfBeers, notification.OnUtc);
            _documentSession.Store(entry);
            _documentSession.SaveChanges();
            return Task.CompletedTask;
        }

        public Task Handle(AllEntriesRemovedNotification notification, CancellationToken cancellationToken)
        {
            _documentSession.Advanced.DocumentStore.Operations
                .Send(new DeleteByQueryOperation(new IndexQuery
                {
                    Query = "from TooManyBeersDatas"
                }));

            return Task.CompletedTask;
        }
    }
}