using System;

namespace Journaling.ViewModels
{
    public class EntryViewModel
    {
        public DateTime Date { get; }
        public int NumOfBeers { get; }
        public EntryViewModel(DateTime forDate, int numOfBeers)
        {
            Date = forDate;
            NumOfBeers = numOfBeers;
        }

        public override string ToString()
        {
            return $"{Date}: {NumOfBeers}";
        }

    }
}