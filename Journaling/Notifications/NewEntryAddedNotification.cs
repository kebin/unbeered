﻿using System;
using MediatR;

namespace Journaling.Notifications
{
    public class NewEntryAddedNotification : INotification
    {
        public DateTime ForDate { get; set; }
        public int NumOfBeers { get; set; }
    }

    public class HadTooManyBeersNotification : INotification
    {
        public DateTime OnUtc { get; set; }
        public int NumOfBeers { get; set; }
    }

    public class HadBeersConsecutivelyNotification : INotification
    {
        public int CurrentStreak { get; set; }
        public DateTime StreakStartedOnUtc { get; set; }
        public DateTime OnUtc { get; set; }
    }
}