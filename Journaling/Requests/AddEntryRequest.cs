﻿using System;
using MediatR;

namespace Journaling.Requests
{
    public class AddEntryRequest : IRequest
    {
        public DateTime ForDate { get; set; }
        public int NumOfBeers { get; set; }
    }
}