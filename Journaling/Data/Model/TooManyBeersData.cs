﻿using System;

namespace Journaling.Data.Model
{
    public class TooManyBeersData
    {
        private TooManyBeersData() { }

        public string Id { get; private set; }
        public int NumOfBeers { get; private set; }
        public DateTime OnUtc { get; private set; }

        public static TooManyBeersData Create(int numOfBeers, DateTime onUtc) => new TooManyBeersData
        {
            Id = $"toomanybeers/{onUtc:yyyyMMdd}",
            NumOfBeers = numOfBeers,
            OnUtc = onUtc
        };

    }

}