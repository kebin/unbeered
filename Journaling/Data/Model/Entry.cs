﻿using System;
using EnsureThat;

namespace Journaling.Data.Model
{
    internal class Entry
    {
        private Entry() { }
        public string Id { get; private set; }
        public int Count { get; set; }
        public DateTime ForDateInUtc { get; set; }
        
        public static Entry Create(DateTime forDateInUtc, int count)
        {
            var firstDayOf2018 = new DateTime(2018, 1, 1);
            var tomorrowInUtc = DateTime.UtcNow.AddDays(1).Date;

            var validatedDate = EnsureArg.IsInRange(forDateInUtc, firstDayOf2018, tomorrowInUtc);
            var validatedCount = EnsureArg.IsGt(count, -1);

            var entry = new Entry
            {
                ForDateInUtc = validatedDate,
                Id = $"entries/{validatedDate:yyyyMMdd}",
                Count = validatedCount
            };
            return entry;
        }
    }

    public class CalculatedData
    {
        public string Id { get; set; }
        public int TotalNumberOfBeers { get; set; }

        public static string GetId => "calculatedData";
    }
}