﻿using System;

namespace Journaling.Data.Model
{
    public class BeeredConsecutivelyData
    {
        public string Id { get; set; }
        public int Streak { get; set; }
        public DateTime FromUtc { get; set; }
        public DateTime ToUtc { get; set; }
    }
}