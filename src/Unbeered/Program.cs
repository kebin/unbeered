﻿using System;
using System.Collections.Generic;
using System.Linq;
using Journaling.Data;
using Journaling.Data.Model;
using Journaling.Handlers;
using Journaling.Requests;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Session;
using Raven.Client.Exceptions;
using Raven.Client.Exceptions.Database;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;
using Tababular;

namespace Unbeered
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Getting Unbeered!");
            Console.WriteLine("--------------------------------------");

            var serviceProvider = _getServiceProvider();
            var mediatr = serviceProvider.GetService<IMediator>();
            var documentSession = serviceProvider.GetService<IDocumentSession>();

            // reset database by deleting all entries!
            mediatr.Send(new RemoveAllEntriesRequest());

            // last week
            var datesForMissingEntries = GetDatesForMissingEntries(DateTime.UtcNow.AddDays(-7));

            foreach (var date in datesForMissingEntries)
            {
                Console.Write($"# of beers on {date:yyyy MMMM dd}: ");
                var numOfBeersDrankThisDay = int.Parse(Console.ReadLine());
                mediatr.Send(new AddEntryRequest
                {
                    ForDate = date.ToUniversalTime(),
                    NumOfBeers = numOfBeersDrankThisDay
                });
            }

            Console.Clear();

            var calculatedData = documentSession
                .Query<CalculatedData>()
                .Single(d=>d.Id == CalculatedData.GetId);

            var tooManyBeersCount = documentSession
                .Query<TooManyBeersData>()
                .Count();
            
            var beeredConsecutively = documentSession
                .Query<BeeredConsecutivelyData>()
                .Count();
            
            var objects = new[]
            {
                new  { Remarks = "Total Beers", Total = calculatedData.TotalNumberOfBeers},
                new  { Remarks = "Total days of Too Many Beers", Total = tooManyBeersCount},
                new  { Remarks = "Total Beered Consecutively", Total = beeredConsecutively},
            };

            
            var streakCount = 0;
            var consecutiveEntries = new List<object>();
            foreach (var entry in documentSession
                .Query<BeeredConsecutivelyData>())
            {
                streakCount++;
                consecutiveEntries.Add(new { Comment =  $"{streakCount}: {entry.Streak} days streak from {entry.FromUtc:MMMM dd} to {entry.ToUtc:MMMM dd}"});
            }

            var mainData = new TableFormatter()
                .FormatObjects(objects);
            
            var streakData = new TableFormatter()
                .FormatObjects(consecutiveEntries);

            Console.WriteLine(mainData);
            Console.WriteLine(streakData);

            Console.ReadLine();
        }

        private static IServiceProvider _getServiceProvider()
        {
            var store = new DocumentStore
            {
                Urls = new[] {"http://localhost:8080"},
                Database = "unbeering-db"
            }
                .Initialize()
                .EnsureDatabaseExistsAsync();

            var services = new ServiceCollection();
            services.AddSingleton(store);
            services.AddTransient(t => store.OpenSession());
            services.AddMediatR(typeof(AddEntryHandler));

            var provider = services.BuildServiceProvider();
            return provider;
        }
        
        private static IDocumentStore EnsureDatabaseExistsAsync(this IDocumentStore store, string database = null, bool createDatabaseIfNotExists = true)
        {
            database = database ?? store.Database;

            if (string.IsNullOrWhiteSpace(database))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(database));

            try
            {
                store.Maintenance.ForDatabase(database).Send(new GetStatisticsOperation());
                return store;
            }
            catch (DatabaseDoesNotExistException)
            {
                if (createDatabaseIfNotExists == false)
                    throw;

                try
                {
                    store.Maintenance.Server.Send(new CreateDatabaseOperation(new DatabaseRecord(database)));
                }
                catch (ConcurrencyException)
                {
                    // The database was already created before calling CreateDatabaseOperation
                }

            }

            return store;
        }
        
        public static IList<DateTime> GetDatesForMissingEntries(DateTime sinceDateTime)
        {
            var sinceFromDate = sinceDateTime.Date;
            var numOfDaysUntilToday = DateTime.UtcNow.Date.Subtract(sinceFromDate).Days;

            var allPossibleEntryDates = new List<DateTime>();
            for (var i = 0; i < numOfDaysUntilToday; i++)
            {
                allPossibleEntryDates.Add(sinceFromDate.AddDays(i));
            }
            return allPossibleEntryDates;
        }


    }
}
